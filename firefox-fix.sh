#!/bin/bash
##########################################################
#                          ,
#                 |'.             ,
#                 |  '-._        / )
#               .'  .._  ',     /_'-,
#              '   /  _'.'_\   /._)')
#             :   /  '_' '_'  /  _.'
#             |E |   |Q| |Q| /   /
#            .'  _\  '-' '-'    /
#          .'--.(S     ,__` )  /
#                '-.     _.'  /
#              __.--'----(   /
#          _.-'     :   __\ /
#         (      __.' :'  :Y
#          '.   '._,  :   : |
#            '.     ) :.__: |
#              \    \______/
#               '._L/_H____]
#                /_        /
#               /  '-.__.-')
#              :      /   /
#              :     /   /
#            ,/_____/----;
#            '._____)----'
#            /     /   /
#           /     /   /
#         .'     /    \
#        (______(-.____)
#
#   Développé par Sebastien TONDELIER - Le 17/01/2022 ######
######       Mozilla Firefox Fix - Disable http3.     ######

## Variables
currentuser=`stat -f "%Su" /dev/console`

## Modify Firefox template so if a new Firefox profile loads, the settings will carry over

if [ ! -e /Applications/Firefox.app ]; then
    echo "Firefox Not Installed"; else
echo "pref("general.config.filename", "firefox.cfg");" >> /Applications/Firefox.app/Contents/Resources/defaults/pref/all.js
echo "pref("general.config.obscure_value", 0);" >> /Applications/Firefox.app/Contents/Resources/defaults/pref/all.js
echo "pref("network.http.http3.enabled", false);" >> /Applications/Firefox.app/Contents/Resources/defaults/pref/all.js

fi

## Modify Firefox settings
## Firefox needs to be closed to make the changes

PROCESS=Firefox
number=$(ps aux | grep $PROCESS | wc -l)

if [ $number -ge 2 ]
    then
        echo "Firefox Running. Prompt user to close Firefox to Continue"
        # Prompt to close FireFox

TITLE="Une mise à jour Firefox est requis"
MSG="Cliquez sur le bouton Mettre à jour pour fermer Firefox. La mise à jour prend 15 secondes et ne redémarre pas votre ordinateur."

POPUP=`/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -title "$TITLE" -description "$MSG" -button1 "Update" -icon /Library/LC/Logo.icns`
    if [ "$POPUP" == "0" ]; then
        # CLOSE FIREFOX
        killall "firefox"
        sleep 10
        # Update configuration file
echo 'user_pref("network.http.http3.enabled", false);' >> /Users/$currentuser/Library/Application\ Support/Firefox/Profiles/*.default*/prefs.js
        sleep 10
        # Relaunch Firefox
        open -a Firefox.app
     fi
fi

if [ $number == 1 ]; then
        sleep 10
        #Firefox not open. Update configuration file
echo 'user_pref("network.http.http3.enabled", false);' >> /Users/$currentuser/Library/Application\ Support/Firefox/Profiles/*.default*/prefs.js

fi

exit 0
