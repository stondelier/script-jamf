#!/bin/bash

# ——————————— Changes Site ———————————
apiURL="https://xxxx.jamfcloud.com"
apiUser="******"
apiPass="******"
newSite="name_site"
idSite="2"

# ——————————— Getting the serial ———————————
computerserial="$(system_profiler SPHardwareDataType | grep 'Serial Number (system)' | awk '{print $NF}')"


#Change the Site
echo "<computer><general><site><id>$idSite</id><name>$newSite</name></site></general></computer>" | curl -X PUT -fku $apiUser:$apiPass -d @- $apiURL/JSSResource/computers/serialnumber/$computerserial/subset/general -H "Content-Type: application/xml"
